<?php

namespace App\Transformers;

use App\Models\Seller;
use League\Fractal\TransformerAbstract;

class SellerTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Seller $seller)
    {
        return [
            'identifier' => (int)$seller->id,
            'name' => $seller->name,
            'email' => $seller->email,
            'isVerified' => (bool)$seller->isVerified(),
            'creationDate' => $seller->created_at,
            'lastChangeDate' => $seller->updated_at,
            'deletionDate' => $seller->deleted_at ?? null,

            'links' => [
                [
                    'rel' => 'self',
                    'href' => route('sellers.show', $seller->id)
                ],
                [
                    'rel' => 'sellers.products',
                    'href' => route('sellers.products.index', $seller->id)
                ],
                [
                    'rel' => 'seller.buyer',
                    'href' => route('sellers.buyers.index', $seller->id)
                ],
                [
                    'rel' => 'sellers.category',
                    'href' => route('sellers.categories.index', $seller->id)
                ],
                [
                    'rel' => 'sellers.transactions',
                    'href' => route('sellers.transactions.index', $seller->id)
                ],
                [
                    'rel' => 'user',
                    'href' => route('users.show', $seller->id)
                ],
            ],
        ];
    }

    public static function getOriginalAttribute(string $transformedAttribute) {
        $attribute = [
            'identifier' => 'id',
            'name' => 'name',
            'email' => 'email',
            'isVerified' => 'verified',
            'cerateionDate' => 'created_at',
            'lastChangeDate' => 'updated_at',
            'deletionDate' => 'deletged_at'
        ];

        return $attribute[$transformedAttribute] ?? null;
    }
}
