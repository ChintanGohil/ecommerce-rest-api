<?php

namespace App\Http\Transformer;

class TransactionTransformer {
    public static function getOriginalAttribute(string $transformedAttribute) {
        $attribute = [
            'identifier' => 'id',
            'quantity' => 'quantity',
            'buyer' => 'buyer_id',
            'product' => 'product_id',
            'creationDate' => 'created_at',
            'lastChangeDate' => 'updated_at',
            'deletionDate' => 'deletged_at'
        ];

        return $attribute[$transformedAttribute] ?? null;
    }
}


?>
