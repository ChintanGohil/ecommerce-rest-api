<?php

namespace App\Http\Transformer;

class BuyerTransformer {
    public static function getOriginalAttribute(string $transformedAttribute) {
        $attribute = [
            'identifier' => 'id',
            'name' => 'name',
            'email' => 'email',
            'isVerified' => 'verified',
            'cerateionDate' => 'created_at',
            'lastChangeDate' => 'updated_at',
            'deletionDate' => 'deletged_at'
        ];

        return $attribute[$transformedAttribute] ?? null;
    }
}


?>
