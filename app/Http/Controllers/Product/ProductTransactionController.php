<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\ApiController;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductTransactionController extends ApiController
{
    // public function index()
    // {
    //     $products = Product::all();
    //     return $this->showAll($products);
    // }

    // public function show(Product $product)
    // {
    //     return $this->showOne($product);
    // }

    public function index(Product $product) {
        $transaction = $product->transactions;
        return $this->showAll($transaction);
    }
}
