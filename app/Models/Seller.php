<?php

namespace App\Models;

use App\Scopes\SellerScope;
use App\Transformers\SellerTransformer;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Seller extends User
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'users';

    public string $transformer = SellerTransformer::class;

    public function __construct() {
        array_push($this->hidden, 'admin');
    }

    protected static function boot() {
        parent::boot();
        static::addGlobalScope(new SellerScope());
    }

    public function products() {
        return $this->hasMany(Product::class);
    }
}
