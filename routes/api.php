<?php

use App\Http\Controllers\Buyer\BuyerCategoryController;
use App\Http\Controllers\Buyer\BuyerProductController;
use App\Http\Controllers\Buyer\BuyersController;
use App\Http\Controllers\Buyer\BuyerSellerController;
use App\Http\Controllers\Buyer\BuyerTransactionController;
use App\Http\Controllers\Category\CategoryBuyerController;
use App\Http\Controllers\Category\CategoryController;
use App\Http\Controllers\Category\CategoryProductController;
use App\Http\Controllers\Category\CategorySellerController;
use App\Http\Controllers\Category\CategoryTransactionController;
use App\Http\Controllers\Product\ProductBuyerController;
use App\Http\Controllers\Product\ProductBuyerTransactionController;
use App\Http\Controllers\Product\ProductCategoryController;
use App\Http\Controllers\Product\ProductsController;
use App\Http\Controllers\Product\ProductTransactionController;
use App\Http\Controllers\Seller\SellerBuyerController;
use App\Http\Controllers\Seller\SellerCategoryController;
use App\Http\Controllers\Seller\SellerProductController;
use App\Http\Controllers\Seller\SellersController;
use App\Http\Controllers\Seller\SellerTransactionController;
use App\Http\Controllers\Transaction\TransactionCategoryController;
use App\Http\Controllers\Transaction\TransactionsController;
use App\Http\Controllers\Transaction\TransactionSellerController;
use App\Http\Controllers\User\UsersController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use SebastianBergmann\Diff\Output\DiffOnlyOutputBuilder;

// Route::resource('buyers', BuyersController::class, ['only' => ['index', 'show']]);


Route::resource('categories', CategoryController::class)->except('craete', 'edit');
Route::resource('category.products', CategoryProductController::class)->only('index');
Route::resource('category.sellers', CategorySellerController::class)->only('index');
Route::resource('category.transactions', CategoryTransactionController::class)->only('index');
Route::resource('category.buyers', CategoryBuyerController::class)->only('index');


Route::resource('products', ProductsController::class)->only('index', 'show');

Route::resource('transactions', TransactionsController::class)->only('index', 'show');
Route::resource('transactions.categories', TransactionCategoryController::class)->only('index');
Route::resource('transactions.sellers', TransactionSellerController::class)->only('index');


Route::resource('buyers', BuyersController::class)->only('index', 'show');
Route::resource('buyers.transactions', BuyerTransactionController::class)->only('index');
Route::resource('buyers.products', BuyerProductController::class)->only('index');
Route::resource('buyers.sellers', BuyerSellerController::class)->only('index');
Route::resource('buyers.categories', BuyerCategoryController::class)->only('index');

Route::resource('sellers', SellersController::class)->only('index', 'show');
Route::resource('sellers.transactions', SellerTransactionController::class)->only('index');
Route::resource('sellers.categories', SellerCategoryController::class)->only('index');
Route::resource('sellers.buyers', SellerBuyerController::class)->only('index');
Route::resource('sellers.products', SellerProductController::class)->except('edit', 'create', 'show');


Route::resource('products', ProductsController::class)->only('index', 'show');
Route::resource('products.buyers', ProductBuyerController::class)->only('index');
Route::resource('products.categories', ProductCategoryController::class)->only('index', 'update', 'destroy');
Route::resource('products.transactions', ProductTransactionController::class)->only('index');
Route::resource('products.buyers.transactions', ProductBuyerTransactionController::class)->only('store');

Route::resource('users', UsersController::class);
Route::get('users/verify/{token}', [UsersController::class, 'verify'])->name('users.verify');
Route::get('users/{user}/rersend-verification-email', [UsersController::class, 'resend'])->name('users.resend');
